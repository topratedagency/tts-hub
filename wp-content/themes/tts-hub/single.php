<?php
/**
 * The template for displaying all single posts
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
get_header();

get_template_part( 'components/hero-blog' );
?>
    <section class="sidebar-page-container blog-details">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-lg-8 col-md-12 col-sm-12 content-side">
                    <div class="blog-details-content">
                        <div class="inner-box">
							<?php while ( have_posts() ) {
								the_post();
								get_template_part( 'loop-templates/content', 'single' );
							}
							?>
                        </div>
						<?php /*if ( comments_open() || get_comments_number() ) : ?>
						<div class="comments-form-area">
	                            <?php comments_template(); ?>
 						</div>
                            <?php }*/ ?>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 sidebar-side">
                    <div class="blog-sidebar">
	                    <?php dynamic_sidebar( 'right-sidebar' ); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
get_footer();
