<?php
/**
 * The template for displaying search results pages
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
get_template_part( 'components/hero' );
$vacancy = isset( $_POST['post_type'] ) && ! empty( $_POST['post_type'] );
?>
<?php if ( $vacancy ) : ?>
    <section class="findjob-section">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-12">
	                <?php if ( ! have_posts() ) : ?>
                        <h1 class="mb-5"><?php echo tts_translate(
				                'Ничего не найдено, попробуйте другую поисковую фразу',
				                'Nothing found, please try a different search phrase',
				                'Нічого не знайдено, спробуйте іншу пошукову фразу' ) ?></h1>
	                <?php endif; ?>
                    <div class="search-inner">
                        <form action="<?php echo esc_url( home_url( '/' ) ); ?>" method="post" class="search-form">
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-12 col-sm-12 column">
                                    <div class="form-group">
                                        <i class="flaticon-search"></i>
                                        <input type="search" id="s" name="s" placeholder="Поиск..." required=""
                                               value="<?php the_search_query(); ?>">
                                        <input type="hidden" name="post_type" value="vacancy"/>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12 column">
                                    <div class="form-group message-btn">
                                        <button type="submit" class="theme-btn-one">Поиск вакансий</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-12">
					<?php if ( have_posts() ) {
						while ( have_posts() ) {
							the_post();
							get_template_part( 'components/job-item' );
						}
					}
					?>
                </div>
            </div>
            <div class="more-btn centred"><?php understrap_pagination(); ?></div>
        </div>
    </section>
<?php else: ?>
    <section class="sidebar-page-container blog-grid">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-lg-8 col-md-12 col-sm-12 content-side">
                    <div class="blog-grid-content">
                        <div class="row clearfix">
							<?php if ( have_posts() ) {
								while ( have_posts() ) {
									the_post();
									get_template_part( 'loop-templates/content', get_post_format() );
								}
							} else {
								get_template_part( 'loop-templates/content', 'none' );
							}
							?>
                        </div>
                        <div class="more-btn centred"><?php understrap_pagination(); ?></div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 sidebar-side">
                    <div class="blog-sidebar">
						<?php dynamic_sidebar( 'right-sidebar' ); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
<?php
get_footer();
