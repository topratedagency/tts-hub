<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

// Removes the parent themes stylesheet and scripts from inc/enqueue.php
function understrap_remove_scripts() {
	wp_dequeue_style( 'understrap-styles' );
	wp_deregister_style( 'understrap-styles' );

	wp_dequeue_script( 'understrap-scripts' );
	wp_deregister_script( 'understrap-scripts' );
}

add_action( 'wp_enqueue_scripts', 'understrap_remove_scripts', 20 );

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {

	// Get the theme data
	$the_theme = wp_get_theme();
//    wp_enqueue_style( 'child-understrap-styles', get_stylesheet_directory_uri() . '/css/child-theme.min.css', array(), $the_theme->get( 'Version' ) );
//    wp_enqueue_script( 'jquery');
//    wp_enqueue_script( 'child-understrap-scripts', get_stylesheet_directory_uri() . '/js/child-theme.min.js', array(), $the_theme->get( 'Version' ), true );
	wp_enqueue_style( 'font-awesome-all', get_stylesheet_directory_uri() . '/assets/css/font-awesome-all.css', array() );
	wp_enqueue_style( 'flaticon', get_stylesheet_directory_uri() . '/assets/css/flaticon.css', array() );
	wp_enqueue_style( 'owl', get_stylesheet_directory_uri() . '/assets/css/owl.css', array() );
	wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri() . '/assets/css/bootstrap.css', array() );
	wp_enqueue_style( 'fancybox', get_stylesheet_directory_uri() . '/assets/css/jquery.fancybox.min.css', array() );
	wp_enqueue_style( 'animate.css', get_stylesheet_directory_uri() . '/assets/css/animate.css', array() );
	wp_enqueue_style( 'nice-select.css', get_stylesheet_directory_uri() . '/assets/css/nice-select.css', array() );
	wp_enqueue_style( 'color.css', get_stylesheet_directory_uri() . '/assets/css/color.css', array() );
	wp_enqueue_style( 'style.css', get_stylesheet_directory_uri() . '/assets/css/style.css', array() );
	wp_enqueue_style( 'responsive.css', get_stylesheet_directory_uri() . '/assets/css/responsive.css', array() );
	wp_enqueue_style( 'child-styles', get_stylesheet_directory_uri() . '/css/child-theme.min.css', array() );

	wp_enqueue_script( 'jquery.js', get_stylesheet_directory_uri() . '/assets/js/jquery.js', array(), '', true );
	wp_enqueue_script( 'popper.min.js', get_stylesheet_directory_uri() . '/assets/js/popper.min.js', array(), '', true );
	wp_enqueue_script( 'bootstrap.min.js', get_stylesheet_directory_uri() . '/assets/js/bootstrap.min.js', array(), '', true );
	wp_enqueue_script( 'owl.js', get_stylesheet_directory_uri() . '/assets/js/owl.js', array(), '', true );
	wp_enqueue_script( 'wow.js', get_stylesheet_directory_uri() . '/assets/js/wow.js', array(), '', true );
	wp_enqueue_script( 'validation.js', get_stylesheet_directory_uri() . '/assets/js/validation.js', array(), '', true );
	wp_enqueue_script( 'jquery.fancybox.js', get_stylesheet_directory_uri() . '/assets/js/jquery.fancybox.js', array(), '', true );
	wp_enqueue_script( 'TweenMax.min.js', get_stylesheet_directory_uri() . '/assets/js/TweenMax.min.js', array(), '', true );
	wp_enqueue_script( 'appear.js', get_stylesheet_directory_uri() . '/assets/js/appear.js', array(), '', true );
	wp_enqueue_script( 'jquery.countTo.js', get_stylesheet_directory_uri() . '/assets/js/jquery.countTo.js', array(), '', true );
	wp_enqueue_script( 'scrollbar.js', get_stylesheet_directory_uri() . '/assets/js/scrollbar.js', array(), '', true );
	wp_enqueue_script( 'jquery.nice-select.min.js', get_stylesheet_directory_uri() . '/assets/js/jquery.nice-select.min.js', array(), '', true );
	wp_enqueue_script( 'isotope.js', get_stylesheet_directory_uri() . '/assets/js/isotope.js', array(), '', true );
	wp_enqueue_script( 'tilt.jquery.js', get_stylesheet_directory_uri() . '/assets/js/tilt.jquery.js', array(), '', true );
	wp_enqueue_script( 'script.js', get_stylesheet_directory_uri() . '/assets/js/script.js', array(), '', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

function add_child_theme_textdomain() {
	load_child_theme_textdomain( 'understrap-child', get_stylesheet_directory() . '/languages' );
}

add_action( 'after_setup_theme', 'add_child_theme_textdomain' );

add_filter( 'use_block_editor_for_post', '__return_false', 10 );

/**
 * ACF Page builder
 */
function tts_page_builder() {
	if ( ! is_front_page() && ! is_single() && ! is_page_template() ) {
		get_template_part( 'components/hero' );
	}
	// check if the flexible content field has rows of data
	if ( have_rows( 'page_builder' ) ):
		// loop through the rows of data
		while ( have_rows( 'page_builder' ) ) : the_row();
			if ( 'slider' == get_row_layout() ):
				get_template_part( 'components/slider' );
            elseif ( 'tiles' == get_row_layout() ):
				get_template_part( 'components/tiles' );
            elseif ( 'about_founder' == get_row_layout() ):
				get_template_part( 'components/about_founder' );
            elseif ( 'services_list' == get_row_layout() ):
				get_template_part( 'components/services_list' );
            elseif ( 'employerjobseeker' == get_row_layout() ):
				get_template_part( 'components/employer_jobseeker' );
            elseif ( 'news_categories' == get_row_layout() ):
				get_template_part( 'components/news_categories' );
            elseif ( 'news_posts' == get_row_layout() ):
				get_template_part( 'components/news_posts' );
            elseif ( 'contact_us' == get_row_layout() ):
				get_template_part( 'components/contact_us' );
            elseif ( 'simple_content' == get_row_layout() ):
				get_template_part( 'components/simple_content' );
            elseif ( 'clients' == get_row_layout() ):
				get_template_part( 'components/clients' );
            elseif ( 'partners' == get_row_layout() ):
				get_template_part( 'components/partners' );
            elseif ( 'recommendations' == get_row_layout() ):
				get_template_part( 'components/recommendations' );
            elseif ( 'arguments' == get_row_layout() ):
				get_template_part( 'components/arguments' );
            elseif ( 'contact_form' == get_row_layout() ):
				get_template_part( 'components/contact_form' );
            elseif ( 'how_we_work' == get_row_layout() ):
				get_template_part( 'components/how_we_work' );
            elseif ( 'start_cooperation' == get_row_layout() ):
				get_template_part( 'components/start_cooperation' );
            elseif ( 'content_box' == get_row_layout() ):
				get_template_part( 'components/content_box' );
            elseif ( 'vacancies' == get_row_layout() ):
				get_template_part( 'components/vacancies' );
            elseif ( 'testimonials' == get_row_layout() ):
				get_template_part( 'components/testimonials' );
			endif;
		endwhile;
	else :
		echo '<p class="text-center mb-5">No components added</p>';
	endif;
}

function tts_image( $image_id, $size = 'full' ) {
	return wp_get_attachment_image( $image_id, $size );
}

function tts_image_url( $image_id, $size = 'full' ) {
	return wp_get_attachment_image_url( $image_id, $size );
}

/**
 * Add ACF options page
 */
if ( function_exists( 'acf_add_options_page' ) ) {
	acf_add_options_page( array(
		'page_title' => 'Theme General Settings',
		'menu_title' => 'Theme Settings',
		'menu_slug'  => 'theme-general-settings',
		'capability' => 'edit_posts',
		'redirect'   => false
	) );
}

function tts_translate( $ru, $en, $ua ) {
	if ( 'ru' === ICL_LANGUAGE_CODE ) {
		return $ru;
	} elseif ( 'en' === ICL_LANGUAGE_CODE ) {
		return $en;
	} else {
		return $ua;
	}
}

add_image_size( 'service-preview', 740, 600, true );
add_image_size( 'slider', 1920, 832, true );
add_image_size( 'tiles', 600, 428, true );
add_image_size( 'photo', 800, 1110, true );
add_image_size( 'blog', 370, 490, true );
add_image_size( 'big', 922, 752, true );

/**
 * Thumbnail upscale
 *
 * @param $default
 * @param $orig_w
 * @param $orig_h
 * @param $new_w
 * @param $new_h
 * @param $crop
 *
 * @return array|null
 */
add_filter( 'image_resize_dimensions', function ( $default, $orig_w, $orig_h, $new_w, $new_h, $crop ) {
	if ( ! $crop ) {
		return null;
	} // let the wordpress default function handle this

	$aspect_ratio = $orig_w / $orig_h;
	$size_ratio   = max( $new_w / $orig_w, $new_h / $orig_h );

	$crop_w = round( $new_w / $size_ratio );
	$crop_h = round( $new_h / $size_ratio );

	$s_x = floor( ( $orig_w - $crop_w ) / 2 );
	$s_y = floor( ( $orig_h - $crop_h ) / 2 );

	return array( 0, 0, (int) $s_x, (int) $s_y, (int) $new_w, (int) $new_h, (int) $crop_w, (int) $crop_h );
}, 10, 6 );

function tts_share() {
	$url         = urlencode( get_permalink() );
	$title       = str_replace( ' ', '%20', get_the_title() );
	$twitterURL  = 'https://twitter.com/intent/tweet?text=' . $title . '&amp;url=' . $url;
	$facebookURL = 'https://www.facebook.com/sharer/sharer.php?u=' . $url;
	$linkedInURL = 'https://www.linkedin.com/sharing/share-offsite/?url=' . $url;
	$mailURL     = 'mailto:vk@tts-hub.com?subject=' . $title . '&body=' . $url;
	$telegram    = 'https://t.me/share/url?url=' . $url . '&text=' . $title;
	$onclick     = 'onclick="javascript:window.open(this.href,\'\', \'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600\');return false;"';
	?>
    <div class="social-box">
        <ul class="social-links clearfix">
            <li><a href="<?php echo $facebookURL; ?>" <?php echo $onclick; ?> class="facebook"><i
                            class="fab fa-facebook-f"></i>Facebook</a></li>
            <li><a href="<?php echo $linkedInURL; ?>" <?php echo $onclick; ?> class="linkedin"><i
                            class="fab fa-linkedin-in"></i>Linkedin</a></li>
            <li><a href="<?php echo $telegram; ?>" <?php echo $onclick; ?> class="telegram"><i
                            class="fab fa-telegram"></i>Telegram</a>
            </li>
        </ul>
    </div>
<?php }