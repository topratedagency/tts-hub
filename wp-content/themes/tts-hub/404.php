<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$title  = tts_translate( 'Извините, эта страница не найдена', 'Sorry this page isn’t available', 'Вибачте, ця сторінка не знайдена' );
$desc   = tts_translate( 'Мы не можем найти страницу, которую вы ищете', 'We\'re not being able to find the page you\'re looking for', 'Ми не можемо знайти сторінку, яку ви шукаєте' );
$button = tts_translate( 'На главную страницу', 'Back to Home', 'На головну сторiнку' );
?>
    <section class="error-section centred">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-lg-10 col-md-12 col-sm-12 offset-lg-1 content-column">
                    <div class="error-content">
                        <figure class="image-box"><img
                                    src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/error-img.png"
                                    alt=""></figure>
                        <h2><?php echo $title; ?></h2>
                        <p class="text-center"><?php echo $desc; ?></p>
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>"
                           class="theme-btn-two"><?php echo $button; ?></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php
get_footer();
