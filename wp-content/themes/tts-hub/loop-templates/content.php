<?php
/**
 * Post rendering content according to caller of get_template_part
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
$month = get_the_date( 'M' );
$day   = get_the_date( 'd' );
$cat = get_the_category_list();
?>
<div class="col-lg-6 col-md-6 col-sm-12 news-block">
    <article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
        <div class="news-block-one wow fadeInUp animated" data-wow-duration="1500ms">
            <div class="inner-box">
                <figure class="image-box">
                    <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'blog' ); ?></a>
                    <span class="post-date"><?php echo $day; ?><br><?php echo $month; ?></span>
                </figure>
                <div class="lower-content">
                    <div class="inner">
                        <div class="category"><i class="flaticon-note"></i>
                            <?php echo $cat; ?></div>
                        <h3>
                            <a href="<?php the_permalink(); ?>">
								<?php the_title(); ?>
                            </a>
                        </h3>
                        <ul class="post-info clearfix">
                            <li><i class="far fa-user"></i>Harley Reuban</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </article>
</div>