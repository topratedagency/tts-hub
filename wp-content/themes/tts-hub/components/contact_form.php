<section class="contact-section">
    <div class="auto-container">
        <div class="sec-title centred">
			<?php if ( ! empty ( $subtitle = get_sub_field( 'subtitle' ) ) ): ?>
                <span class="top-title"><?php echo $subtitle; ?></span>
			<?php endif; ?>
			<?php if ( ! empty ( $title = get_sub_field( 'title' ) ) ): ?>
                <h2><?php echo $title; ?></h2>
			<?php endif; ?>
        </div>
		<?php if ( ! empty ( $form = get_sub_field( 'form' ) ) ): ?>
            <div class="default-form">
				<?php echo do_shortcode( $form ) ?>
            </div>
		<?php endif; ?>
    </div>
</section>