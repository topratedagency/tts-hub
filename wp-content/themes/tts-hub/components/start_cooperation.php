<?php
$title = get_sub_field( 'title' );
if ( have_rows( 'start_cooperation' ) ): ?>
    <section class="feature-section">
		<?php if ( ! empty ( $title ) ): ?>
            <div class="sec-title centred light">
                <h2><?php echo $title; ?></h2>
            </div>
		<?php endif; ?>
        <div class="outer-container">
            <div id="content_block_4">
                <div class="content-box">
                    <div class="inner-box">
                        <div class="row clearfix">
							<?php while ( have_rows( 'start_cooperation' ) ) : the_row();
								$icon     = tts_image( get_sub_field( 'icon' ) );
								$text     = get_sub_field( 'text' );
								$delay    = get_row_index() * 50;
								?>
                                <div class="col-lg-4 col-md-6 col-sm-12 single-column">
                                    <div class="single-item wow fadeInRight animated animated"
                                         data-wow-delay="<?php echo $delay; ?>ms"
                                         data-wow-duration="1500ms">
                                        <div class="inner">
                                            <figure class="icon-box">
												<?php echo $icon; ?>
                                            </figure>
                                            <p><?php echo $text; ?></p>
                                        </div>
                                    </div>
                                </div>
							<?php endwhile; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>