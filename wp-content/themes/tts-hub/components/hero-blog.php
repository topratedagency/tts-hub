<?php
$bg           = get_field( 'custom_hero_image' ) ? tts_image_url( get_field( 'custom_hero_image' ) ) : tts_image_url( get_field( 'default_hero_image', 'options' ), 'slider' );
$title        = get_field( 'custom_title' ) ? get_field( 'custom_title' ) : get_the_title();
$month        = get_the_date( 'M' );
$day          = get_the_date( 'd' );
$cat_list     = get_the_category_list();
$cat_list_ids = wp_list_pluck( get_the_category(), 'term_id' );
$hide_date    = in_array( 8, $cat_list_ids );
?>
<section class="page-title" style="background-image: url(<?php echo $bg; ?>);">
    <div class="pattern-layer"
         style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/shape/pattern-35.png);"></div>
    <div class="auto-container">
        <div class="post-box">
            <div class="news-block-one">
                <div class="inner-box">
                    <div class="lower-content">
                        <div class="inner">
							<?php if ( ! $hide_date ): ?>
                                <span class="post-date"><?php echo $day; ?><br><?php echo $month; ?></span>
							<?php endif; ?>
                            <div class="category"><i class="flaticon-note"></i><?php echo $cat_list; ?></div>
                            <h2><?php echo $title; ?></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>