<?php
$title = get_sub_field( 'title' );
$ppp   = get_sub_field( 'number_of_posts' );
$cat   = get_sub_field( 'display_category' );
?>
<section class="news-section news-posts">
    <div class="auto-container">
        <div class="sec-title">
			<?php if ( ! empty ( $title ) ): ?>
                <h2><?php echo $title; ?></h2>
			<?php endif; ?>
        </div>
		<?php $arg = array(
			'post_type'      => 'post',
			'order'          => 'DESC',
			'orderby'        => 'date',
			'posts_per_page' => $ppp,
			'cat'            => $cat
		);
		$the_query = new WP_Query( $arg );
		if ( $the_query->have_posts() ) : ?>
            <div class="row">
				<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <div class="col-lg-4 col-md-4 col-sm-12 news-block">
                        <div class="news-block-one">
                            <div class="inner-box">
                                <figure class="image-box">
                                    <a href="<?php the_permalink(); ?>">
										<?php the_post_thumbnail('blog'); ?>
                                    </a>
                                </figure>
                                <div class="lower-content">
                                    <a href="<?php the_permalink(); ?>">
                                        <div class="inner">
                                            <h3><?php the_title(); ?></h3>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
				<?php endwhile; ?>
            </div>
		<?php endif;
		wp_reset_query(); ?>
    </div>
</section>