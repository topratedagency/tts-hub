<?php
$title = get_sub_field( 'title' );
$bgc   = get_sub_field( 'background_color' ) ? 'style="background-color:' . get_sub_field( 'background_color' ) . '"' : '';
if ( have_rows( 'how_we_work' ) ): ?>
    <section class="process-section centred" <?php echo $bgc; ?>>
        <div class="auto-container">
			<?php if ( ! empty ( $title ) ): ?>
                <div class="sec-title centred light">
                    <h2><?php echo $title; ?></h2>
                </div>
			<?php endif; ?>
            <div class="row clearfix">
				<?php while ( have_rows( 'how_we_work' ) ) : the_row();
					$icon     = tts_image( get_sub_field( 'icon' ) );
					$text     = get_sub_field( 'text' );
					$position = get_row_index();
					$delay    = get_row_index() * 50;
					?>
                    <div class="col-lg-4 col-md-6 col-sm-12 process-block">
                        <div class="process-block-one wow fadeInUp animated animated"
                             data-wow-delay="<?php echo $delay; ?>ms"
                             data-wow-duration="1500ms">
                            <div class="inner-box">
                                <figure class="icon-box">
									<?php echo $icon; ?>
                                    <span><?php echo $position; ?></span>
                                    <div class="anim-icon">
                                        <div class="icon-1"
                                             style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/shape/pattern-8.png);"></div>
                                        <div class="icon-2 rotate-me"
                                             style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/anim-icon-2.png);"></div>
                                        <div class="icon-3 rotate-me"
                                             style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/anim-icon-2.png);"></div>
                                    </div>
                                </figure>
								<?php if ( ! empty ( $text ) ): ?>
                                    <div class="lower-content">
                                        <p><?php echo $text; ?></p>
                                    </div>
								<?php endif; ?>
                            </div>
                        </div>
                    </div>
				<?php endwhile; ?>
            </div>
        </div>
    </section>
<?php endif; ?>