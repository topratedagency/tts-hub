<?php if ( have_rows( 'tiles' ) ): ?>
    <section class="my-5 about-section about-section--tts">
        <div class="auto-container">
            <div class="inner-container">
				<?php while ( have_rows( 'tiles' ) ) : the_row();
					$index        = get_row_index();
					$reverse      = $index % 2 ? 'flex-row-reverse' : '';
					$title        = get_sub_field( 'title' );
					$description  = get_sub_field( 'description' );
					$image_url    = tts_image_url( get_sub_field( 'image' ), 'tiles' );
					$image        = tts_image( get_sub_field( 'image' ) );
					$button_label = get_sub_field( 'link_text' );
					$button_url   = get_sub_field( 'link_url' );
					?>
                    <div class="row clearfix <?php echo $reverse; ?>">
                        <div class="col-lg-6 col-md-12 col-sm-12 content-column col-img"
                             style="background-image: url(<?php echo $image_url; ?>);">
                            <div class="content_block_2">
                                <div class="content-box centred">
                                    <figure class="image-box"><?php echo $image; ?></figure>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 content-column">
                            <div class="content_block_3">
                                <div class="content-box">
                                    <div class="sec-title">
										<?php if ( ! empty ( $title ) ): ?>
                                            <h2><?php echo $title; ?></h2>
										<?php endif; ?>
                                    </div>
                                    <div class="text">
										<?php if ( ! empty ( $description ) ): ?>
											<?php echo $description; ?>
										<?php endif; ?>
										<?php if ( ! empty ( $button_label ) && ! empty( $button_url ) ): ?>
                                            <br>
                                            <div class="link">
                                                <a href="<?php echo $button_url; ?>"><i
                                                            class="flaticon-right-arrow"></i><?php echo $button_label; ?>
                                                </a>
                                            </div>
										<?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
				<?php endwhile; ?>
            </div>
        </div>
    </section>
<?php endif; ?>