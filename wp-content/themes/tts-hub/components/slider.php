<?php if ( have_rows( 'slider' ) ): ?>
    <section class="banner-section style-three">
        <div class="banner-carousel owl-theme owl-carousel owl-dots-none">
			<?php while ( have_rows( 'slider' ) ) : the_row();
				$title        = get_sub_field( 'title' );
				$subtitle     = get_sub_field( 'subtitle' );
				$description  = get_sub_field( 'description' );
				$image        = tts_image_url( get_sub_field( 'image' ), 'slider' );
				$button_label = get_sub_field( 'button_label' );
				$button_url   = get_sub_field( 'button_url' );
				?>
                <div class="slide-item">
                    <div class="image-layer"
                         style="background-image:url(<?php echo $image; ?>)"></div>
                    <div class="pattern-layer">
                        <div class="pattern-1"
                             style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/shape/pattern-26.png);"></div>
                        <div class="pattern-2"
                             style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/shape/pattern-27.png);"></div>
                        <div class="pattern-3"
                             style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/shape/pattern-28.png);"></div>
                        <div class="pattern-4"
                             style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/shape/pattern-29.png);"></div>
                        <div class="pattern-5"
                             style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/shape/pattern-30.png);"></div>
                    </div>
                    <div class="auto-container">
                        <div class="row clearfix">
                            <div class="col-lg-10 col-md-12 col-sm-12 offset-lg-2 content-column">
                                <div class="content-box">
									<?php if ( ! empty ( $title ) ): ?>
                                        <h1><?php echo $title; ?></h1>
									<?php endif; ?>
									<?php if ( ! empty ( $subtitle ) ): ?>
                                        <h2><?php echo $subtitle; ?></h2>
									<?php endif; ?>
									<?php if ( ! empty ( $description ) ): ?>
										<?php echo $description; ?>
									<?php endif; ?>
									<?php if ( ! empty ( $button_label ) && ! empty( $button_url ) ): ?>
                                        <div class="btn-box">
                                            <a href="<?php echo $button_url; ?>"
                                               class="btn-one"><?php echo $button_label; ?></a>
                                        </div>
									<?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			<?php endwhile; ?>
        </div>
        <div class="social-box">
			<?php if ( have_rows( 'social_links', 'options' ) ): ?>
                <ul class="social-links">
					<?php while ( have_rows( 'social_links', 'options' ) ) : the_row(); ?>
                        <li>
                            <a target="_blank"
                               href="<?php the_sub_field( 'url' ); ?>"><?php the_sub_field( 'icon' ); ?></a>
                        </li>
					<?php endwhile; ?>
                </ul>
			<?php endif; ?>
        </div>
    </section>
<?php endif; ?>