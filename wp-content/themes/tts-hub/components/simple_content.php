<?php if ( ! empty ( $content = get_sub_field( 'content' ) ) ):
	$bgc = get_sub_field( 'background_color' ) ? 'style="background-color:' . get_sub_field( 'background_color' ) . '"' : '';
	?>
    <section class="tts-inner-page-section" <?php echo $bgc; ?>>
        <div class="auto-container">
			<?php echo $content; ?>
        </div>
        <div class="clearfix"></div>
    </section>
<?php endif; ?>