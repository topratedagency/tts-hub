<?php
$before_title = get_sub_field( 'before_title' );
$title        = get_sub_field( 'title' );
$subtitle     = get_sub_field( 'subtitle' );
$tpl          = get_page_template_slug();
?>
<section class="findjob-section">
    <div class="auto-container">
        <div class="sec-title centred">
			<?php if ( ! empty ( $before_title ) ): ?>
                <span class="top-title"><?php echo $before_title; ?></span>
			<?php endif; ?>
			<?php if ( ! empty ( $title ) ): ?>
                <h2><?php echo $title; ?></h2>
			<?php endif; ?>
			<?php if ( ! empty ( $subtitle ) ): ?>
                <p class="text-center"><?php echo $subtitle; ?></p>
			<?php endif; ?>
        </div>
		<?php if ( $tpl !== 'page-templates/clean.php' ): ?>
            <div class="search-inner">
                <form action="<?php echo esc_url( home_url( '/' ) ); ?>" method="post" class="search-form">
                    <div class="row clearfix">
                        <div class="col-lg-6 col-md-12 col-sm-12 column">
                            <div class="form-group">
                                <i class="flaticon-search"></i>
                                <input type="search" id="s" name="s" placeholder="Поиск..." required=""
                                       value="<?php the_search_query(); ?>">
                                <input type="hidden" name="post_type" value="vacancy"/>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 column">
                            <div class="form-group message-btn">
                                <button type="submit" class="theme-btn-one">Поиск вакансий</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
		<?php endif; ?>
		<?php
		$arg = array(
			'post_type'      => 'vacancy',
			'post_status'    => 'publish',
			'order'          => 'DESC',
			'orderby'        => 'date',
			'paged'          => get_query_var( 'paged' ),
			'posts_per_page' => get_option( 'posts_per_page' )
		);
		query_posts( $arg );
		if ( have_posts() ) : ?>
            <div class="post-jobs">
				<?php while ( have_posts() ) : the_post(); ?>
                    <div class="single-job-post">
						<?php get_template_part( 'components/job-item' ); ?>
                    </div>
				<?php endwhile; ?>
            </div>
		<?php endif; ?>
        <div class="more-btn centred"><?php understrap_pagination(); ?></div>
		<?php wp_reset_query(); ?>
    </div>
</section>