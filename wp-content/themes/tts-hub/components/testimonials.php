<?php $arg = array(
	'post_type'      => 'testimonial',
	'order'          => 'DESC',
	'orderby'        => 'menu_order',
	'posts_per_page' => - 1
);
$the_query = new WP_Query( $arg );
if ( $the_query->have_posts() ) : ?>
    <section class="testimonial-page-section">
        <div class="auto-container">
            <div class="row">
                <div class="col-12">
                    <div class="sec-title centred">
                        <h2><?php echo tts_translate( 'Отзывы', 'Testimonials', 'Відгуки' ) ?></h2>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
				<?php while ( $the_query->have_posts() ) : $the_query->the_post();
					$position    = get_field( 'position' );
					$testimonial = get_field( 'testimonial' );
					$has_thumb   = has_post_thumbnail();
					$class       = ! $has_thumb ? 'no-thumb' : '';
					?>
                    <div class="col-lg-4 col-md-6 col-sm-12 testimonial-block">
                        <div class="testimonial-block-three">
                            <div class="inner-box">
                                <figure class="quote-box"><img
                                            src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/quote-3.png"
                                            alt=""></figure>
                                <div class="author-box <?php echo $class; ?>">
									<?php if ( $has_thumb ): ?>
                                        <figure class="author-thumb">
											<?php the_post_thumbnail( 'thumbnail' ); ?>
                                        </figure>
									<?php endif; ?>
                                    <h3><?php the_title(); ?></h3>
									<?php if ( ! empty ( $position ) ): ?>
                                        <span class="designation"><?php echo $position; ?></span>
									<?php endif; ?>
                                </div>
								<?php if ( ! empty ( $testimonial ) ): ?>
                                    <div class="text">
                                        <p><?php echo $testimonial; ?></p>
                                    </div>
								<?php endif; ?>
                            </div>
                        </div>
                    </div>
				<?php endwhile; ?>
            </div>
        </div>
    </section>
<?php endif;
wp_reset_query(); ?>