<?php
$title     = get_sub_field( 'title' );
$arg       = array(
	'post_type'      => 'client',
	'order'          => 'ASC',
	'orderby'        => 'menu_order',
	'posts_per_page' => - 1
);
$the_query = new WP_Query( $arg ); ?>
<?php if ( $the_query->have_posts() ) : ?>
    <section id="clients" class="clients-section alternet-2">
		<?php if ( ! empty ( $title ) ): ?>
            <div class="sec-title light centred">
                <h2><?php echo $title; ?></h2>
            </div>
		<?php endif; ?>
        <div class="outer-container">
            <div class="clients-carousel owl-carousel owl-theme owl-dots-none owl-nav-none">
				<?php while ( $the_query->have_posts() ) : $the_query->the_post();
					?>
                    <figure class="clients-logo-box">
						<?php the_post_thumbnail( 'medium' ); ?>
                    </figure>
				<?php endwhile; ?>
            </div>
        </div>
    </section>
<?php endif;
wp_reset_query(); ?>