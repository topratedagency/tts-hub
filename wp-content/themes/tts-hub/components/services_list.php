<?php
$layout   = get_sub_field( 'layout' );
$title    = get_sub_field( 'title' );
$subtitle = get_sub_field( 'subtitle' );
$icon     = get_sub_field( 'icon' );
$url      = get_sub_field( 'page_url' );
$more     = tts_translate( 'Подробнее', 'Learn more', 'Детальнiше' );
$arg      = array(
	'post_type'      => 'service',
	'order'          => 'ASC',
	'orderby'        => 'menu_order',
	'posts_per_page' => - 1
);
if ( is_singular( 'service' ) ) {
	$arg['post__not_in'] = array( get_the_ID() );
}
$the_query = new WP_Query( $arg ); ?>
<?php if ( $the_query->have_posts() ) : ?>
	<?php if ( 'hp' === $layout ):
		global $post; ?>
        <section class="service-section bg-color-2">
            <div class="auto-container">
                <div class="sec-title light centred">
					<?php if ( ! empty ( $subtitle ) ): ?>
                        <span class="top-title"><?php echo $subtitle; ?></span>
					<?php endif; ?>
					<?php if ( ! empty ( $title ) ): ?>
                        <h2><?php echo $title; ?></h2>
					<?php endif; ?>
                </div>
                <div class="tts-services row">
					<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <div class="col-lg-4 col-md-6 col-sm-12 service-block-one">
                            <div class="inner-box">
                                <figure class="image-box"><?php the_post_thumbnail( 'service-preview' ); ?></figure>
                                <div class="lower-content">
                                    <div class="content-box">
                                        <div class="inner">
                                            <figure class="icon-box"><?php echo tts_image( get_field( 'icon' ) ) ?></figure>
                                            <h4><?php the_title(); ?></h4>
                                        </div>
                                    </div>
                                    <div class="overlay-content">
                                        <a href="<?php echo $url . '#' . $post->post_name; ?>"><i
                                                    class="flaticon-right-arrow"></i><?php echo $more; ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
					<?php endwhile; ?>
                </div>
            </div>
        </section>
	<?php else: ?>
        <section class="tts-inner-page-section">
            <div class="container">
                <div class="row">
					<?php
					$i = 0;
					while ( $the_query->have_posts() ) : $the_query->the_post();
						global $post;
						$reverse = $i % 2 ? '' : 'flex-row-reverse';
						$bg      = $i % 2 ? 'service-bg' : '';
						?>
                        <div id="<?php echo $post->post_name; ?>" class="blog-details-content col-12 col-md-6">
                            <div class="author-box centred">
                                <div class="before"
                                     style="background-image: url(<?php the_post_thumbnail_url( 'slider' ); ?>);"></div>
                                <div class="inner">
                                    <figure class="image-box"></figure>
                                    <h3><?php the_title() ?></h3>
									<?php the_content(); ?>
                                </div>
                            </div>
                        </div>
						<?php $i ++;
					endwhile; ?>
                </div>
            </div>
        </section>
	<?php endif; ?>
<?php endif;
wp_reset_query(); ?>