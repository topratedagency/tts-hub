<?php
$recommendations = get_sub_field( 'recommendations' );
$title           = get_sub_field( 'title' );
?>
<?php if ( ! empty( $recommendations ) ): ?>
    <section id="recommendations" class="tts-gallery container y-section_inner">
        <div class="auto-container">
			<?php if ( ! empty ( $title ) ): ?>
                <div class="sec-title light centred">
                    <h2><?php echo $title; ?></h2>
                </div>
			<?php endif; ?>
            <div class="row">
				<?php foreach ( $recommendations as $image ) : ?>
                    <div class="col-6 col-sm-3">
                        <a data-fancybox="gallery" data-src="<?php echo esc_url( $image['url'] ); ?>"
                           data-caption="<?php echo esc_html( $image['caption'] ); ?>">
                            <img class="img-fluid" src="<?php echo esc_url( $image['sizes']['medium'] ); ?>"
                                 alt="<?php echo esc_attr( $image['alt'] ); ?>">
                        </a>
                    </div>
				<?php endforeach; ?>
            </div>
        </div>
    </section>
<?php endif;