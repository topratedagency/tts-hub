<?php
$home  = tts_translate( 'Главная', 'Home', 'Головна' );
$bg    = get_field( 'custom_hero_image' ) ? tts_image_url( get_field( 'custom_hero_image' ) ) : tts_image_url( get_field( 'default_hero_image', 'options' ), 'slider' );
$title = get_field( 'custom_title' ) ? get_field( 'custom_title' ) : get_the_title();
?>
<section class="page-title" style="background-image: url(<?php echo $bg; ?>);">
    <div class="pattern-layer"
         style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/shape/pattern-35.png);"></div>
    <div class="auto-container">
        <div class="row">
            <div class="content-box col-12">
                <div class="title-box centred">
					<?php if ( is_archive() ): ?>
						<?php the_archive_title( '<h1>', '</h1>' ); ?>
					<?php elseif ( is_search() ): ?>
                        <h1>
							<?php
							printf(
							/* translators: %s: query term */
								esc_html__( 'Search Results for: %s', 'understrap' ),
								'<span>' . get_search_query() . '</span>'
							);
							?>
                        </h1>
					<?php elseif ( is_home() ): ?>
                        <h1>Пресс-центр</h1>
					<?php else: ?>
                        <h1><?php echo $title; ?></h1>
						<?php if ( ! empty ( $add_subtitle = get_field( 'add_subtitle' ) ) ): ?>
                            <p><?php echo $add_subtitle; ?></p>
						<?php endif; ?>
					<?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>