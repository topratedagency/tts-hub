<?php
$title        = get_sub_field( 'title' );
$subtitle     = get_sub_field( 'subtitle' );
$description  = get_sub_field( 'description' );
$image        = tts_image( get_sub_field( 'photo' ), 'photo' );
$button_label = get_sub_field( 'button_label' );
$button_url   = get_sub_field( 'button_url' );
?>
<section id="about" class="recruitment-technology tts-about">
    <div class="auto-container">
        <div class="row clearfix">
            <div class="col-lg-4 col-md-4 col-sm-12 image-column">
                <figure class="clearfix">
					<?php echo $image; ?>
                </figure>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-12 content-column right">
                <div id="content_block_4">
                    <div class="content-box">
                        <div class="sec-title">
							<?php if ( ! empty ( $title ) ): ?>
                                <h2><?php echo $title; ?></h2>
							<?php endif; ?>
	                        <?php if ( ! empty ( $subtitle ) ): ?>
                                <span class="top-title"><?php echo $subtitle; ?></span>
	                        <?php endif; ?>
							<?php if ( ! empty ( $description ) ): ?>
								<?php echo $description; ?>
							<?php endif; ?>
							<?php if ( ! empty ( $button_label ) && ! empty( $button_url ) ): ?>
                                <p><a href="<?php echo $button_url; ?>"
                                      class="theme-btn-one"><?php echo $button_label; ?></a></p>
							<?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
