<?php
$title               = get_sub_field( 'title' );
$split_titles        = explode( '/', $title );
$subtitle            = get_sub_field( 'subtitle' );
$image               = tts_image( get_sub_field( 'image' ), 'large' );
$title_e             = get_sub_field( 'title_e' );
$subtitle_e          = get_sub_field( 'subtitle_e' );
$description_e       = get_sub_field( 'description_e' );
$link_text_e         = get_sub_field( 'link_text_e' );
$link_url_e          = get_sub_field( 'link_url_e' );
$send_request_text_e = get_sub_field( 'send_request_text_e' );
$send_request_form_e = get_sub_field( 'send_request_form_e' );
$title_j             = get_sub_field( 'title_j' );
$subtitle_j          = get_sub_field( 'subtitle_j' );
$description_j       = get_sub_field( 'description_j' );
$link_text_j         = get_sub_field( 'link_text_j' );
$link_url_j          = get_sub_field( 'link_url_j' );
$send_request_text_j = get_sub_field( 'send_request_text_j' );
$send_request_form_j = get_sub_field( 'send_request_form_j' );
?>
<section class="welcome-section tts-welcome">
    <div class="auto-container">
        <div class="sec-title centred">
			<?php if ( ! empty ( $subtitle ) ): ?>
                <span class="top-title"><?php echo $subtitle; ?></span>
			<?php endif; ?>
			<?php if ( ! empty ( $title ) ): ?>
                <h2><?php echo $title; ?></h2>
			<?php endif; ?>
        </div>
        <div class="row clearfix">
            <div class="col-lg-6 col-md-12 col-sm-12 image-column">
				<?php if ( ! empty ( $image ) ): ?>
                    <figure class="image-box js-tilt" style="will-change: transform;">
						<?php echo $image; ?>
                        <div class="js-tilt-glare">
                            <div class="js-tilt-glare-inner"></div>
                        </div>
                    </figure>
				<?php endif; ?>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 content-column">
                <div id="content_block_1">
                    <div class="content-box">
                        <div class="tabs-box">
                            <div class="tab-btn-box">
                                <ul class="tab-btns tab-buttons clearfix">
                                    <li class="tab-btn active-btn" data-tab="#tab-1">
                                        <i class="employ-icon flaticon-businessman"></i>
										<?php if ( isset( $split_titles[0] ) ): ?>
                                            <h5><?php echo $split_titles[0]; ?></h5>
										<?php endif; ?>
                                        <i class="arrow-icon flaticon-up-arrow-2"></i>
                                    </li>
                                    <li class="tab-btn" data-tab="#tab-2">
                                        <i class="employ-icon flaticon-employer"></i>
										<?php if ( isset( $split_titles[1] ) ): ?>
                                            <h5><?php echo $split_titles[1]; ?></h5>
										<?php endif; ?>
                                        <i class="arrow-icon flaticon-up-arrow-2"></i>
                                    </li>
                                </ul>
                            </div>
                            <div class="tabs-content">
                                <div class="tab active-tab" id="tab-1">
                                    <div class="inner-box">
										<?php if ( ! empty ( $title_e ) ): ?>
                                            <h5><?php echo $title_e; ?></h5>
										<?php endif; ?>
										<?php if ( ! empty ( $subtitle_e ) ): ?>
                                            <h2><?php echo $subtitle_e; ?></h2>
										<?php endif; ?>
										<?php if ( ! empty ( $description_e ) ): ?>
											<?php echo $description_e; ?>
										<?php endif; ?>
										<?php if ( ! empty ( $link_text_e ) && ! empty( $link_url_e ) ): ?>
                                            <div class="link">
                                                <a href="<?php echo $link_url_e; ?>"><i
                                                            class="flaticon-right-arrow"></i><?php echo $link_text_e; ?>
                                                </a>
                                            </div>
										<?php endif; ?>
										<?php if ( ! empty ( $send_request_form_e ) ): ?>
                                            <br>
                                            <ul class="list clearfix">
                                                <li>
                                                    <a data-fancybox data-src="#employer" href="javascript:;">
                                                        <figure class="icon-box"><img
                                                                    src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/icons/icon-1.png"
                                                                    alt=""></figure>
														<?php if ( ! empty ( $send_request_text_e ) ): ?>
                                                            <h4><?php echo $send_request_text_e; ?></h4>
														<?php endif; ?>
                                                    </a>
                                                </li>
                                            </ul>
										<?php endif; ?>
                                    </div>
                                </div>
                                <div class="tab" id="tab-2">
                                    <div class="inner-box">
										<?php if ( ! empty ( $title_j ) ): ?>
                                            <h5><?php echo $title_j; ?></h5>
										<?php endif; ?>
										<?php if ( ! empty ( $subtitle_j ) ): ?>
                                            <h2><?php echo $subtitle_j; ?></h2>
										<?php endif; ?>
										<?php if ( ! empty ( $description_j ) ): ?>
											<?php echo $description_j; ?>
										<?php endif; ?>
										<?php if ( ! empty ( $link_text_j ) && ! empty( $link_url_j ) ): ?>
                                            <div class="link">
                                                <a href="<?php echo $link_url_j; ?>"><i
                                                            class="flaticon-right-arrow"></i><?php echo $link_text_j; ?>
                                                </a>
                                            </div>
										<?php endif; ?>
										<?php if ( ! empty ( $send_request_form_j ) ): ?>
                                            <br>
                                            <ul class="list clearfix">
                                                <li>
                                                    <a data-fancybox data-src="#jobseeker" href="javascript:void();">
                                                        <figure class="icon-box"><img
                                                                    src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/icons/icon-1.png"
                                                                    alt=""></figure>
														<?php if ( ! empty ( $send_request_text_j ) ): ?>
                                                            <h4><?php echo $send_request_text_j; ?></h4>
														<?php endif; ?>
                                                    </a>
                                                </li>
                                            </ul>
										<?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php if ( ! empty ( $send_request_form_e ) ): ?>
    <div class="request-form default-form" style="display: none;" id="employer">
		<?php echo do_shortcode( $send_request_form_e ) ?>
    </div>
<?php endif; ?>
<?php if ( ! empty ( $send_request_form_j ) ): ?>
    <div class="request-form default-form" style="display: none;" id="jobseeker">
		<?php echo do_shortcode( $send_request_form_j ) ?>
    </div>
<?php endif; ?>
