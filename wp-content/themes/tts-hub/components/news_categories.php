<?php
$title            = get_sub_field( 'title' );
$subtitle         = get_sub_field( 'subtitle' );
$display_all_text = get_sub_field( 'display_all_text' );
$categories       = get_sub_field( 'display_categories' );
?>
<section class="news-section bg-color-2">
    <div class="pattern-layer"
         style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/shape/pattern-11.png);"></div>
    <div class="auto-container">
        <div class="sec-title">
			<?php if ( ! empty ( $subtitle ) ): ?>
                <span class="top-title"><?php echo $subtitle; ?></span>
			<?php endif; ?>
			<?php if ( ! empty ( $title ) ): ?>
                <h2><?php echo $title; ?></h2>
			<?php endif; ?>
			<?php if ( ! empty ( $display_all_text ) ): ?>
                <a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>" class="link"><i
                            class="flaticon-right-arrow"></i><?php echo $display_all_text; ?></a>
			<?php endif; ?>
        </div>
		<?php if ( ! empty ( $categories ) ): ?>
            <div class="row clearfix">
				<?php foreach ( $categories as $cat_id ) :
					$cat = get_category( $cat_id );
					$link = get_category_link( $cat->term_id );
					$image = tts_image( get_field( 'image', $cat ), 'blog' );
					?>
                    <div class="col-lg-4 col-md-4 col-sm-12 news-block">
                        <div class="news-block-one">
                            <div class="inner-box">
                                <figure class="image-box">
                                    <a href="<?php echo $link; ?>">
										<?php echo $image; ?>
                                    </a>
                                </figure>
                                <div class="lower-content">
                                    <a href="<?php echo $link; ?>">
                                        <div class="inner">
                                            <h3><?php echo $cat->name; ?></h3>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
				<?php endforeach; ?>
            </div>
		<?php endif; ?>
    </div>
</section>