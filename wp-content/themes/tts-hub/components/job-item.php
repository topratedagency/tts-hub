<?php
$experience = get_field( 'experience' );
$salary     = get_field( 'salary' );
?>
<div class="single-job-post">
    <div class="job-header clearfix">
        <ul class="info pull-left">
            <li title="<?php echo tts_translate( 'Опубликовано', 'Published', 'Опубліковано' ) ?>"><i
                        class="flaticon-clock"></i><?php echo get_the_date(); ?></li>
        </ul>
        <div class="number pull-right"><p>ID <?php echo tts_translate( 'Вакансии', 'of Vacancy', 'Вакансії' ) ?>:
				<?php the_ID(); ?></p></div>
    </div>
    <div class="row align-items-stretch job-inner clearfix">
        <div class="col job-title">
            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
        </div>
		<?php if ( ! empty ( $salary ) ): ?>
            <div class="col salary-box">
                <span><?php echo tts_translate( 'Зарплата', 'Salary', 'Заробітна плата' ) ?></span>
                <p><?php echo $salary; ?></p>
            </div>
		<?php endif; ?>
		<?php if ( ! empty ( $experience ) ): ?>
            <div class="col experience-box">
                <span><?php echo tts_translate( 'Опыт работы', 'Experience', 'Досвід' ) ?></span>
                <p><?php echo $experience; ?></p>
            </div>
		<?php endif; ?>
        <div class="col apply-btn">
            <a href="<?php the_permalink(); ?>"><?php echo tts_translate( 'Отправить резюме', 'Apply', 'Надіслати резюме' ) ?></a>
        </div>
    </div>
</div>