<?php
$title     = get_sub_field( 'title' );
$arg       = array(
	'post_type'      => 'partner',
	'order'          => 'ASC',
	'orderby'        => 'menu_order',
	'posts_per_page' => - 1
);
$the_query = new WP_Query( $arg ); ?>
<?php if ( $the_query->have_posts() ) : ?>
    <section id="partners" class="clients-section alternet-2">
		<?php if ( ! empty ( $title ) ): ?>
            <div class="sec-title light centred">
                <h2><?php echo $title; ?></h2>
            </div>
		<?php endif; ?>
        <div class="outer-container">
            <div class="container">
                <div class="row">
					<?php while ( $the_query->have_posts() ) : $the_query->the_post();
						$url = get_field( 'url' );
						?>
                        <div class="col-6 col-sm-3 clients-logo-box">
                            <figure>
								<?php if ( ! empty ( $url ) ): ?>
                                <a href="<?php echo $url; ?>" target="_blank">
									<?php endif; ?>
									<?php the_post_thumbnail( 'medium' ); ?>
									<?php if ( ! empty ( $url ) ): ?>
                                </a>
							<?php endif; ?>
								<?php if ( ! empty ( $url ) ): ?>
                                    <span class="logo-title"><a
                                                href="<?php echo $url; ?>"><?php the_title(); ?></a></span>
								<?php endif; ?>
                            </figure>
                        </div>
					<?php endwhile; ?>
                </div>
            </div>
        </div>
    </section>
<?php endif;
wp_reset_query(); ?>