<?php
$title       = get_sub_field( 'title' );
$description = get_sub_field( 'description' );
?>
<div class="auto-container">
    <div class="row">
        <div class="blog-details-content tts-author-box col-12">
            <div class="author-box centred">
                <div class="inner">
					<?php if ( ! empty ( $title ) ): ?>
                        <h3><?php echo $title; ?></h3>
					<?php endif; ?>
					<?php if ( ! empty ( $description ) ): ?>
						<?php echo $description; ?>
					<?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>