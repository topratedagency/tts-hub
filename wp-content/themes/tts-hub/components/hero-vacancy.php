<?php
$bg    = get_field( 'custom_hero_image' ) ? tts_image_url( get_field( 'custom_hero_image' ) ) : tts_image_url( get_field( 'default_hero_image', 'options' ), 'slider' );
$title = get_the_title();
?>
<section class="page-title" style="background-image: url(<?php echo $bg; ?>);">
    <div class="pattern-layer"
         style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/shape/pattern-35.png);"></div>
    <div class="auto-container">
        <div class="post-box">
            <div class="news-block-one">
                <div class="inner-box">
                    <div class="lower-content">
                        <div class="inner">
                            <h2><?php echo $title; ?></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>