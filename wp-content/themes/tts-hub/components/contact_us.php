<?php
$title      = get_sub_field( 'title' );
$subtitle   = get_sub_field( 'subtitle' );
$form       = get_sub_field( 'form' );
$form_title = get_sub_field( 'form_title' );
$image      = '';
if ( empty( $form ) ) {
	$image = tts_image_url( get_sub_field( 'image' ), 'big' );
}
$title_o       = get_sub_field( 'title_o' );
$description_o = get_sub_field( 'description_o' );
$title_c       = get_sub_field( 'title_c' );
$description_c = get_sub_field( 'description_c' );
$title_w       = get_sub_field( 'title_w' );
$description_w = get_sub_field( 'description_w' );
?>
<section class="contactinfo-section">
    <div class="image-column"
         style="background-image: url(<?php echo $image; ?>);"></div>
    <div class="auto-container">
        <div class="row align-items-center clearfix">
            <div class="col-lg-4 col-sm-12 content-column">
                <div id="content_block_6">
                    <div class="content-box">
                        <div class="sec-title">
							<?php if ( ! empty ( $subtitle ) ): ?>
                                <span class="top-title"><?php echo $subtitle; ?></span>
							<?php endif; ?>
							<?php if ( ! empty ( $title ) ): ?>
                                <h2><?php echo $title; ?></h2>
							<?php endif; ?>
                        </div>
                        <ul class="info-list clearfix">
                            <li>
                                <figure class="icon-box"><img
                                            src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/icon-39.png"
                                            alt=""></figure>
                                <div class="inner">
									<?php if ( ! empty ( $title_o ) ): ?>
                                        <h4><?php echo $title_o; ?></h4>
									<?php endif; ?>
									<?php if ( ! empty ( $description_o ) ): ?>
										<?php echo $description_o; ?>
									<?php endif; ?>
                                </div>
                            </li>
                            <li>
                                <figure class="icon-box"><img
                                            src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/icon-40.png"
                                            alt=""></figure>
                                <div class="inner">
									<?php if ( ! empty ( $title_c ) ): ?>
                                        <h4><?php echo $title_c; ?></h4>
									<?php endif; ?>
									<?php if ( ! empty ( $description_c ) ): ?>
										<?php echo $description_c; ?>
									<?php endif; ?>
                                </div>
                            </li>
                            <li>
                                <figure class="icon-box"><img
                                            src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icons/icon-41.png"
                                            alt=""></figure>
                                <div class="inner">
									<?php if ( ! empty ( $title_w ) ): ?>
                                        <h4><?php echo $title_w; ?></h4>
									<?php endif; ?>
									<?php if ( ! empty ( $description_w ) ): ?>
										<?php echo $description_w; ?>
									<?php endif; ?>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
			<?php if ( ! empty ( $form ) ): ?>
                <div class="col-lg-8 col-sm-12 form-column">
                    <div class="default-form contact-page-section">
						<?php if ( ! empty ( $form_title ) ): ?>
                            <h2><?php echo $form_title; ?></h2>
						<?php endif; ?>
						<?php echo do_shortcode( $form ); ?>
                    </div>
                </div>
			<?php endif; ?>
        </div>
    </div>
</section>
