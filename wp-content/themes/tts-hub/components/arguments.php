<?php
$bgc = get_sub_field( 'background_color' ) ? 'style="background-color:' . get_sub_field( 'background_color' ) . '"' : '';
?>
<section class="chooseus-section bg-color-2" <?php echo $bgc; ?>>
    <div class="auto-container">
		<?php if ( ! empty ( $title = get_sub_field( 'title' ) ) ): ?>
            <div class="sec-title centred light">
                <h2><?php echo $title; ?></h2>
            </div>
		<?php endif; ?>
		<?php if ( have_rows( 'arguments' ) ): ?>
            <div class="row clearfix">
				<?php while ( have_rows( 'arguments' ) ) : the_row();
					$icon  = tts_image( get_sub_field( 'icon' ) );
					$text  = get_sub_field( 'text' );
					$delay = get_row_index() * 50;
					$position = get_row_index();
					?>
                    <div class="col-lg-4 col-md-6 col-sm-12 chooseus-block">
                        <div class="chooseus-block-one wow fadeInUp animated animated animated"
                             data-wow-delay="<?php echo $delay; ?>ms"
                             data-wow-duration="1500ms">
                            <div class="inner-box">
                                <span><?php echo $position; ?></span>
								<?php if ( ! empty ( $icon ) ): ?>
                                    <figure class="icon-box">
										<?php echo $icon ?>
                                    </figure>
								<?php endif; ?>
								<?php if ( ! empty ( $text ) ): ?>
                                    <p><?php echo $text; ?></p>
								<?php endif; ?>
                            </div>
                        </div>
                    </div>
				<?php endwhile; ?>
            </div>
		<?php endif; ?>
    </div>
</section>