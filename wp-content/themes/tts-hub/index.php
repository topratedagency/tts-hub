<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
get_header();
get_template_part( 'components/hero' );
?>
    <section class="sidebar-page-container blog-grid">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-lg-8 col-md-12 col-sm-12 content-side">
                    <div class="blog-grid-content">
                        <div class="row clearfix">
							<?php if ( have_posts() ) {
								while ( have_posts() ) {
									the_post();
									get_template_part( 'loop-templates/content', get_post_format() );
								}
							} else {
								get_template_part( 'loop-templates/content', 'none' );
							}
							?>
                        </div>
                        <div class="more-btn centred"><?php understrap_pagination(); ?></div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 sidebar-side">
                    <div class="blog-sidebar">
						<?php dynamic_sidebar( 'right-sidebar' ); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
get_footer();