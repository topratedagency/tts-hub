<?php
/**
 * The template for displaying all single posts
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
get_header();
$responsibilities   = get_field( 'responsibilities' );
$education          = get_field( 'education' );
$experience         = get_field( 'experience' );
$skills             = get_field( 'skills' );
$conditions         = get_field( 'conditions' );
$salary             = get_field( 'salary' );
$apply_on_or_before = get_field( 'apply_on_or_before' );
get_template_part( 'components/hero-vacancy' );
?>
    <section class="sidebar-page-container blog-details">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-lg-8 col-md-12 col-sm-12 content-side">
                    <div class="job-details-content">
                        <div class="text">
                            <h2><?php echo tts_translate( 'Описание работы', 'Job Description', 'Описання роботи' ) ?></h2>
							<?php if ( ! empty ( $responsibilities ) ): ?>
                                <h3><?php echo tts_translate( 'Обязанности', 'Responsibilities', 'Обов\'язки' ) ?></h3>
                                <ul class="list clearfix">
									<?php echo preg_replace( '/^(.+)(\s*)$/m', '<li>$1</li>', $responsibilities ); ?>
                                </ul>
							<?php endif; ?>
							<?php if ( ! empty ( $education ) || ! empty( $experience ) || ! empty( $skills ) || ! empty( $conditions ) ): ?>
                                <h3><?php echo tts_translate( 'Требования', 'Requirements', 'Вимоги' ) ?></h3>
                                <ul class="list clearfix">
									<?php if ( ! empty ( $education ) ): ?>
                                        <li>
                                            <span><?php echo tts_translate( 'Образование', 'Education', 'Освіта' ) ?></span>:
											<?php echo $education; ?></li>
									<?php endif; ?>
									<?php if ( ! empty ( $experience ) ): ?>
                                        <li>
                                            <span><?php echo tts_translate( 'Опыт работы', 'Experience', 'Досвід' ) ?></span>:
											<?php echo $experience; ?></li>
									<?php endif; ?>
	                                <?php if ( ! empty ( $skills ) ): ?>
                                        <li>
                                            <span><?php echo tts_translate( 'Навыки и умения', 'Skills', 'Навички' ) ?></span>:
			                                <?php echo $skills; ?></li>
	                                <?php endif; ?>
	                                <?php if ( ! empty ( $conditions ) ): ?>
                                        <li>
                                            <span><?php echo tts_translate( 'Условия', 'Conditions', 'Умови' ) ?></span>:
			                                <?php echo $conditions; ?></li>
	                                <?php endif; ?>
                                </ul>
							<?php endif; ?>
                        </div>
						<?php tts_share(); ?>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 sidebar-side">
                    <div class="job-sidebar">
                        <div class="apply-btn">
                            <a data-fancybox data-src="#jobseeker" href="javascript:void();"
                               class="theme-btn-one"><?php echo tts_translate( 'Отправить резюме', 'Apply for This Job', 'Відправити резюме' ) ?></a>
                        </div>
                        <div class="sidebar-widget job-discription">
                            <ul class="list">
                                <li>
                                    <span>ID <?php echo tts_translate( 'Вакансии', 'of Vacancy', 'Вакансії' ) ?></span>
                                    <p><?php the_ID(); ?></p>
                                </li>
								<?php if ( ! empty ( $salary ) ): ?>
                                    <li>
                                        <span><?php echo tts_translate( 'Зарплата', 'Salary', 'Заробітна плата' ) ?></span>
                                        <p><?php echo $salary; ?></p>
                                    </li>
								<?php endif; ?>
								<?php if ( ! empty ( $apply_on_or_before ) ): ?>
                                    <li>
                                        <span><?php echo tts_translate( 'Подать заявку до', 'Apply on or Before', 'Подати заявку до' ) ?></span>
                                        <p><?php echo $apply_on_or_before; ?></p>
                                    </li>
								<?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </div>
				<?php
				$title     = explode( ' ', get_the_title() );
				$arg       = array(
					'post_type'      => 'vacancy',
					'order'          => 'DESC',
					'orderby'        => 'date',
					's'              => $title[0],
					'sentence'       => false,
					'post__not_in'   => array( get_the_ID() ),
					'posts_per_page' => 3
				);
				$the_query = new WP_Query( $arg );
				if ( $the_query->have_posts() ) : ?>
                    <div class="col-lg-12 col-md-12 col-sm-12 column">
                        <div class="job-details">
                            <div class="related-job">
                                <h2><?php echo tts_translate( 'Похожие вакансии', 'Related Jobs', 'Схожі вакансії' ) ?></h2>
								<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
									<?php get_template_part( 'components/job-item' ); ?>
								<?php endwhile; ?>
                            </div>
                        </div>
                    </div>
				<?php endif;
				wp_reset_query(); ?>
            </div>
        </div>
    </section>
    <div class="request-form default-form" style="display: none;" id="jobseeker">
		<?php echo do_shortcode( '[contact-form-7 id="711" title="Jobseeker form"]' ) ?>
    </div>
<?php
get_footer();
