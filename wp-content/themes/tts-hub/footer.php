<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package UnderStrap
 */
// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>

<footer class="main-footer style-three">
    <div class="auto-container">
        <div class="footer-top">
            <div class="widget-section">
                <div class="row clearfix">
                    <div class="col-md-6 col-sm-12 footer-column">
                        <div class="footer-widget logo-widget">
                            <figure class="footer-logo">
                                <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
									<?php if ( ! empty ( $logo_white = tts_image( get_field( 'logo_white', 'options' ) ) ) ): ?>
										<?php echo $logo_white; ?>
									<?php else: ?>
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo-white.png"
                                             alt="">
									<?php endif; ?>
                                </a>
                            </figure>
							<?php if ( ! empty ( $footer_text = get_field( 'footer_text', 'options' ) ) ): ?>
                                <div class="text">
                                    <p><?php echo $footer_text; ?></p>
                                </div>
							<?php endif; ?>
                        </div>
                        <div class="about-widget">
							<?php if ( have_rows( 'social_links', 'options' ) ): ?>
                                <ul class="social-links clearfix">
									<?php if ( ! empty ( $social_links_title = get_field( 'social_links_title', 'options' ) ) ): ?>
                                        <li><h5><?php echo $social_links_title; ?></h5></li>
									<?php endif; ?>
									<?php while ( have_rows( 'social_links', 'options' ) ) : the_row(); ?>
                                        <li>
                                            <a target="_blank"
                                               href="<?php the_sub_field( 'url' ); ?>"><?php the_sub_field( 'icon' ); ?></a>
                                        </li>
									<?php endwhile; ?>
                                </ul>
							<?php endif; ?>
                        </div>
                    </div>
                    <div class="col-md-5 offset-md-1 offset-sm-0 col-sm-12 footer-column">
                        <div class="footer-widget links-widget">
							<?php if ( ! empty ( $useful_links_title = get_field( 'useful_links_title', 'options' ) ) ): ?>
                                <div class="widget-title">
                                    <h3><?php echo $useful_links_title; ?></h3>
                                </div>
							<?php endif; ?>
							<?php wp_nav_menu( array(
								'container_id'    => '',
								'theme_location'  => 'footer',
								'container_class' => '',
								'menu_id'         => '',
								'menu_class'      => 'links-list clearfix',
								'fallback_cb'     => '',
								'depth'           => 2,
								'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
							) ); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom clearfix">
            <div class="copyright pull-left">
				<?php if ( ! empty ( $copyright_text = get_field( 'copyright', 'options' ) ) ): ?>
                    <p><?php echo $copyright_text; ?></p>
				<?php endif; ?>
            </div>
            <ul class="footer-nav pull-right clearfix">
				<?php if ( ! empty ( $privacy_policy_url = get_field( 'privacy_policy_url', 'options' ) ) ): ?>
                    <li>
                        <a href="<?php echo $privacy_policy_url; ?>"><?php the_field( 'privacy_policy_text', 'options' ) ?></a>
                    </li>
				<?php endif; ?>
				<?php if ( ! empty ( $sitemap = get_field( 'sitemap', 'options' ) ) ): ?>
                    <li><a href="<?php echo $sitemap; ?>"><?php the_field( 'sitemap_text', 'options' ) ?></a></li>
				<?php endif; ?>
            </ul>
        </div>
    </div>
</footer>
<button class="scroll-top scroll-to-target" data-target="html"><i class="flaticon-up-arrow-1"></i>Top</button>
</div><!--boxed_wrapper end-->
<?php wp_footer(); ?>
</body>
</html>