<?php
/**
 * The header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$logo_colored = tts_image( get_field( 'logo_colored', 'options' ) );
$logo_white   = tts_image( get_field( 'logo_white', 'options' ) );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> <?php understrap_body_attributes(); ?>>
<div class="boxed_wrapper">
    <!-- Preloader -->
    <div class="loader-wrap">
        <div class="preloader">
            <!--<div class="preloader-close">Preloader Close</div>-->
        </div>
        <div class="layer layer-one"><span class="overlay"></span></div>
        <div class="layer layer-two"><span class="overlay"></span></div>
        <div class="layer layer-three"><span class="overlay"></span></div>
    </div>
    <!-- main header -->
    <header class="main-header style-three">
        <div class="header-lower">
            <div class="outer-box">
                <figure class="logo-box">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
						<?php if ( is_front_page() ): ?>
							<?php if ( ! empty ( $logo_colored ) ): ?>
								<?php echo $logo_colored; ?>
							<?php else: ?>
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo.png" alt="">
							<?php endif; ?>
						<?php else: ?>
							<?php if ( ! empty ( $logo_white ) ): ?>
								<?php echo $logo_white; ?>
							<?php else: ?>
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo-white.png"
                                     alt="">
							<?php endif; ?>
						<?php endif; ?>
                    </a>
                </figure>
                <div class="menu-area">
                    <!--Mobile Navigation Toggler-->
                    <div class="mobile-nav-toggler">
                        <i class="icon-bar"></i>
                        <i class="icon-bar"></i>
                        <i class="icon-bar"></i>
                    </div>
                    <nav class="main-menu navbar-expand-md navbar-light">
                        <div class="collapse navbar-collapse show clearfix" id="navbarSupportedContent">
							<?php wp_nav_menu( array(
								'container_id'    => '',
								'theme_location'  => 'primary',
								'container_class' => '',
								'menu_id'         => '',
								'menu_class'      => 'navigation clearfix',
								'fallback_cb'     => '',
								'depth'           => 2,
								'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
							) ); ?>
                        </div>
                    </nav>
                </div>
                <ul class="menu-right-content pull-right clearfix">
                    <li>
                        <div class="language">
                            <div class="lang-btn">
                                <i class="icon flaticon-planet-earth"></i>
                                <span class="txt">RU</span>
                                <span class="arrow fa fa-angle-down"></span>
                            </div>
                            <div class="lang-dropdown">
                                <ul>
                                    <li><a href="#">EN</a></li>
                                    <li><a href="#">UA</a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

        <!--sticky Header-->
        <div class="sticky-header">
            <div class="outer-box clearfix">
                <div class="menu-area pull-left">
                    <figure class="logo-box"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img
                                    src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo.png"
                                    alt=""></a></figure>
                    <nav class="main-menu clearfix">
                        <!--Keep This Empty / Menu will come through Javascript-->
                    </nav>
                </div>
                <ul class="menu-right-content pull-right clearfix">
                    <li>
                        <div class="language">
                            <div class="lang-btn">
                                <i class="icon flaticon-planet-earth"></i>
                                <span class="txt">EN</span>
                                <span class="arrow fa fa-angle-down"></span>
                            </div>
                            <div class="lang-dropdown">
                                <ul>
                                    <li><a href="#">EN</a></li>
                                    <li><a href="#">UA</a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </header>
    <!-- main-header end -->

    <!-- Mobile Menu  -->
    <div class="mobile-menu">
        <div class="menu-backdrop"></div>
        <div class="close-btn"><i class="fas fa-times"></i></div>
        <nav class="menu-box">
            <div class="nav-logo"><a href="#"><img
                            src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo-white.png" alt=""
                            title=""></a></div>
            <div class="menu-outer">
                <!--Here Menu Will Come Automatically Via Javascript / Same Menu as in Header--></div>
            <div class="contact-info">
				<?php if ( ! empty ( $contact_info_title = get_field( 'contact_info_title', 'options' ) ) ): ?>
                    <h4><?php echo $contact_info_title; ?></h4>
				<?php endif; ?>
                <ul>
					<?php if ( ! empty ( $address = get_field( 'address', 'options' ) ) ): ?>
                        <li><?php echo $address; ?></li>
					<?php endif; ?>
					<?php if ( ! empty ( $phone = get_field( 'phone', 'options' ) ) ): ?>
                        <li><a href="<?php echo $phone; ?>"><?php echo $phone; ?></li>
					<?php endif; ?>
					<?php if ( ! empty ( $email = get_field( 'email', 'options' ) ) ): ?>
                        <li><a href="<?php echo $email; ?>"><?php echo $email; ?></li>
					<?php endif; ?>
                </ul>
            </div>
			<?php if ( have_rows( 'social_links', 'options' ) ): ?>
                <div class="social-links">
                    <ul class="clearfix">
						<?php while ( have_rows( 'social_links', 'options' ) ) : the_row(); ?>
                            <li><a target="_blank"
                                   href="<?php the_sub_field( 'url' ); ?>"><?php the_sub_field( 'icon' ); ?></a></li>
						<?php endwhile; ?>
                    </ul>
                </div>
			<?php endif; ?>
        </nav>
    </div>
    <!-- End Mobile Menu -->